package wee;


import java.util.Scanner;
        import java.lang.Math;
public class GuessANumber {
    public static void main(String[] args) {
        int UnknownNumber, UserNumber, TrysCount = 0;
        Scanner input = new Scanner(System.in);
        System.out.println("Let the game Begin!!! OBJECTIVE: Guess the number (from 0 to 100).");
        UnknownNumber = (int)Math.floor(Math.random() * 100);
        do {
            TrysCount++;
            System.out.print("Enter your number: ");
            UserNumber = input.nextInt();
            if (UserNumber > UnknownNumber)
                System.out.println("Your number is too big. Please, try again.");
            else if (UserNumber < UnknownNumber) System.out.println("Your number is too small. Please, try again.");
            else System.out.println("You guessed!");
        } while (UserNumber != UnknownNumber);
        System.out.println("Number of attempts: " + TrysCount);
    }
}